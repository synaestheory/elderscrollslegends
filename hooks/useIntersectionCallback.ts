import React from "react";

const useIntersectionCallback = (
  /**
   * The Element to observe for intersection
   */
  ref: React.RefObject<HTMLDivElement | HTMLElement>,
  /**
   * The options object being handed to the IntersectionObserver
   * For description of the properties, see:
   * https://developer.mozilla.org/en-US/docs/Web/API/IntersectionObserver
   */
  options: IntersectionObserverInit,
  /**
   * The callback to be called when the object is intersecting
   */
  callback
) => {
  const [observer, setObserver] = React.useState();

  React.useEffect(() => {
    if (observer && ref.current) {
      observer.unobserve(ref.current);
    }

    const observerHandler: IntersectionObserverCallback = entries => {
      if (entries.some(entry => entry.isIntersecting)) {
        callback();
      }
    };

    const newObserver = new IntersectionObserver(observerHandler, options);

    if (ref.current) {
      newObserver.observe(ref.current);
    }

    setObserver(newObserver);
  }, [callback, ref.current]);
};

export default useIntersectionCallback;
