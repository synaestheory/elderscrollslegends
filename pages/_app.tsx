import React from "react";
import CardCollectionProvider from "../components/CardCollectionProvider";
import GlobalCSS from "../components/GlobalCSS";

/**
 * This component can be seen as the root of the application.  All pages render under it.
 * https://nextjs.org/docs/advanced-features/custom-app
 */
export default ({ Component, pageProps }) => (
  <CardCollectionProvider>
    <GlobalCSS />
    <Component {...pageProps} />
  </CardCollectionProvider>
);
