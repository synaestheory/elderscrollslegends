import React from "react";
import Head from "next/head";
import { NextPage } from "next";
import Card from "../components/Card";
import CardCollection from "../state/CardCollection";
import useInterserctionCallback from "../hooks/useIntersectionCallback";
import FullPageSpinner from "../components/FullPageSpinner";

/**
 * This component is the home page "/".
 * File naming for it is by convention of next.js.
 *
 * A responsive layout is achieved via CSS grid.
 */
const Home: NextPage = () => {
  const { cards, requestPending, fetchNextPage } = CardCollection.useApi();
  const infiniteScrollRef = React.useRef<HTMLDivElement>();
  useInterserctionCallback(infiniteScrollRef, { threshold: 0 }, fetchNextPage);
  return (
    <>
      <Head>
        <title>Cards</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      {requestPending && <FullPageSpinner />}
      <main className="grid">
        {cards.map(card => (
          <Card key={card.id} {...card} />
        ))}
        {requestPending === false && (
          <div className="infiniteScrollTarget" ref={infiniteScrollRef} />
        )}
      </main>
      <style jsx>{`
        .grid {
          display: grid;
          grid-template-columns: repeat(auto-fit, minmax(320px, 1fr));
          grid-gap: 1rem;
        }
      `}</style>
    </>
  );
};

export default Home;
