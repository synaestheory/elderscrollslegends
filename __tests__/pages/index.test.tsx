import playwright from "playwright";
import axios from "axios";

enum Selectors {
  Card = ".card",
  Grid = ".grid",
  Loader = ".loader",
  InfiniteScrollTarget = ".infiniteScrollTarget"
}

const cardsApiRegex = /\/cards/;

describe("Functional tests for the Home Page", () => {
  // note: This is set to 30 seconds only to accommodate testing while on a sub-par connection
  // or in scenarios where the API may be a bit slower to respond. In most cases,
  // it should finish much quicker than this on a stable broadband connection.
  jest.setTimeout(30000);
  const headless = process?.env?.SHOW_BROWSER === undefined;
  const browserTypes = ["firefox", "chromium", "webkit"];
  browserTypes.forEach(browserType => {
    describe(browserType, () => {
      let browser;
      let context;
      let page;
      /**
       * This function gets used throughout tests to wait for images to have loaded.
       * It has been the most reliable method for ensuring a full load state across all browsers.
       */
      const imagesLoaded = () =>
        [...document.querySelectorAll("img")].every(img => img.complete);

      beforeAll(async () => {
        browser = await playwright[browserType].launch({ headless });
        context = await browser.newContext();
      });

      beforeEach(async () => {
        page = await context.newPage("http://localhost:3000");
        page.setViewport({
          width: 1080,
          height: 960
        });
      });

      afterEach(async () => {
        await page.close();
      });

      afterAll(async () => {
        await context.close();
        await browser.close();
      });

      it('should show a "loader" when a request is pending', async () => {
        const loader = await page.waitForSelector(Selectors.Loader);
        expect(await loader.visibleRatio()).toBe(1);
      });

      it("should fetch only 20 cards initially", async () => {
        await page.waitForSelector(Selectors.Card);
        const cards = await page.$$(Selectors.Card);
        expect(cards.length).toBe(20);
      });

      it("should attempt to load from the second page for the second request", async () => {
        await page.waitForSelector(Selectors.Card);
        await page
          .waitForSelector(Selectors.InfiniteScrollTarget)
          .then(target => target.scrollIntoViewIfNeeded());
        const response = await page.waitForResponse(cardsApiRegex);
        expect(response.url()).toMatch("page=2");
      });

      it("should render at minimum a mobile and a desktop layout", async () => {
        await page.waitForSelector(Selectors.Card);
        // wait for all images to load
        await page.waitFor(imagesLoaded);
        const desktopYOffset = await page.$eval(
          Selectors.InfiniteScrollTarget,
          target => target.getBoundingClientRect().y
        );
        const iPhone6Viewport = playwright.devices["iPhone 6"].viewport;
        // passing isMobile false here to prevent playwright from reloading the page
        // The UI will render the same regardless of this setting.
        await page.setViewport({ ...iPhone6Viewport, isMobile: false });
        const mobileYOffset = await page.$eval(
          Selectors.InfiniteScrollTarget,
          target => target.getBoundingClientRect().y
        );
        // Here the intuition is that the mobile view presents a much longer list, vertically
        // Because of that we can safely say that the mobile distance for the infinite scroll target
        // should be further down the page than the desktop version.
        expect(desktopYOffset).toBeLessThan(mobileYOffset);
      });

      it("should append new pages to previously rendered pages", async () => {
        await page.waitForSelector(Selectors.Card);
        await page
          .waitForSelector(Selectors.InfiniteScrollTarget)
          .then(target => target.scrollIntoViewIfNeeded());
        await page.waitForResponse(cardsApiRegex);
        // ensure new images are loaded
        await page.waitFor(imagesLoaded);
        const cards = await page.$$(Selectors.Card);
        expect(cards.length).toBe(40);
      });

      it("should render the image, name, type, set name, and text", async () => {
        // This request is happening external to the page (rather than catching the one that occurs within the page)
        // because the API can occasionally return a 304 status code, which doesnt allow access to the body
        // Making the request this way ensures that we get access to the body, but risks breaking the test if the API
        // changes sort order of its returned results.
        const response = await axios
          .get("https://api.elderscrollslegends.io/v1/cards", {
            params: { pageSize: 1 }
          })
          .then();
        await page.waitForSelector(Selectors.Card);
        await page.waitFor(imagesLoaded);
        const { cards } = await response.data;
        const card = cards[0];
        const cardEl = await page.$(`#${card.id}`);
        const result = {
          image: await cardEl.$eval(
            `img`,
            (img, card) => img.src === card.imageUrl,
            card
          ),
          name: await cardEl.$eval(
            `.name`,
            (name, card) => name.innerText.includes(card.name),
            card
          ),
          type: await cardEl.$eval(
            `.type`,
            (type, card) => type.innerText.includes(card.type),
            card
          ),
          setName: await cardEl.$eval(
            `.setName`,
            (setName, card) => setName.innerText.includes(card.set.name),
            card
          ),
          text: await cardEl.$eval(
            ".text",
            (text, card) => text.innerText.includes(card.text),
            card
          )
        };

        expect(result).toEqual(
          expect.objectContaining({
            image: true,
            name: true,
            type: true,
            setName: true,
            text: true
          })
        );
      });
    });
  });
});
