# Elder Scrolls Legends Card List

This is a UI for cards from the Elder Scrolls Legends game. It is a [next.js application](https://nextjs.org/), which also means it's written in react. State management is modeled through the react hooks API.

# Getting started

### Host machine setup

In order to get this project up and running you only need a recent version of node/npm. It's recommended that you use the latest [lts release](https://nodejs.org/en/download/). You may install it directly or use a node version manager like [nvm](https://github.com/nvm-sh/nvm) or [fnm](https://github.com/Schniz/fnm).

| name | version  |
| ---- | -------- |
| node | lts(12+) |
| npm  | latest   |

### Running the server

The steps to run the project are as follows:

1. Clone the repository.
1. Run `npm ci` to install dependencies from `package-lock.json`.
1. Run `npm run dev`.
1. Open a browser and navigate to [http://localhost:3000](http://localhost:3000) (This was tested in modern versions of firefox, chromium, and webkit).

The server should start up in development mode. This will listen at http://localhost:3000 for incoming requests to render the application, and hot-reload if changes are made to the source code.

### Building the project

You can create a production build of the server by running `npm run build`. Once the build is complete you can use `npm start` to run the build.

### Running the tests

Ideally tests are running as part of a CI/CD system. Additionally, you may want to run the tests locally. Currently, this project only has functional tests. These tests were chosen as they can assure us that the functional requirements of the project are met from a user's perspective.

To run the tests, first ensure the server is running (either via `npm run dev` or `npm run build && npm start`), then in another terminal run `npm test`.

Tests will run against firefox, chromium, and webkit based headless browsers. If you would like to view the browsers in headed mode, simply set the environment variable `SHOW_BROWSER` to any value (e.g. `SHOW_BROWSER=1 npm test`)

The tests are using a pre-1.0 browser running engine called playwright. In production based projects, it is unlikely that I would recommend using a pre-1.0 piece of open source software without having seriously vetted it otherwise and considered other options. However, I do feel that an interview process is a great time to potentially gather and provide feedback for the open source authors/contributors.
