import React from "react";

/**
 * CSS that is intended to be applied globally for the entire application
 */
const GlobalCSS: React.FunctionComponent = () => (
  <>
    <style jsx global>{`
      html,
      body {
        background: #333;
      }
      :root {
        --font-color: #ddd;
      }
    `}</style>
  </>
);

export default GlobalCSS;
