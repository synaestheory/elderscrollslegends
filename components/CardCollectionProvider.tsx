import React from "react";
import CardCollection from "../state/CardCollection";

/**
 * This is a context provider for the CardCollection application state
 * Any component below this can use `CardCollection.useApi()` to get
 * get access to a `CardCollectionApi` object and the values that are
 * exposed from state.
 *
 * @remarks
 * For further implementation details, reference
 * {@link state/CardCollection.ts | state/CardCollection.ts }
 */
const CardCollectionProvider: React.FunctionComponent = props => {
  const [state, dispatch] = React.useReducer(
    CardCollection.Reducer,
    CardCollection.INITIAL_STATE
  );
  return (
    <CardCollection.Context.Provider value={{ state, dispatch }}>
      {props.children}
    </CardCollection.Context.Provider>
  );
};

export default CardCollectionProvider;
