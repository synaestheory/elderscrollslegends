import React from "react";
/**
 * This component is a fixed position full viewport "spinner" or loading indicator.
 *
 * @remarks
 * The .loader class and any pseudo-classes and keyframes associated with it
 * came from {@link https://projects.lukehaas.me/css-loaders/ | css-loaders }
 * The .scrim class is not from that source, it was written for this application by me.
 */
const FullPageSpinner: React.FC = () => {
  return (
    <div className="scrim">
      <div className="loader" />
      <style jsx>{`
        .scrim {
          position: fixed;
          z-index: 1;
          top: 0;
          left: 0;
          width: 100vw;
          height: 100vh;
          background: rgba(22, 22, 22, 0.7);
          display: flex;
          justify-content: center;
          align-items: center;
        }

        // ALL CSS BELOW THIS COMMENT IS COPY PASTED FROM THE FOLLOWING URL
        // Loader Class Styles from https://projects.lukehaas.me/css-loaders/
        // Written by @lukehaas
        .loader,
        .loader:before,
        .loader:after {
          background: #ffffff;
          -webkit-animation: load1 1s infinite ease-in-out;
          animation: load1 1s infinite ease-in-out;
          width: 1em;
          height: 4em;
        }
        .loader {
          color: #ffffff;
          text-indent: -9999em;
          margin: 88px auto;
          position: relative;
          font-size: 11px;
          -webkit-transform: translateZ(0);
          -ms-transform: translateZ(0);
          transform: translateZ(0);
          -webkit-animation-delay: -0.16s;
          animation-delay: -0.16s;
        }
        .loader:before,
        .loader:after {
          position: absolute;
          top: 0;
          content: "";
        }
        .loader:before {
          left: -1.5em;
          -webkit-animation-delay: -0.32s;
          animation-delay: -0.32s;
        }
        .loader:after {
          left: 1.5em;
        }
        @-webkit-keyframes load1 {
          0%,
          80%,
          100% {
            box-shadow: 0 0;
            height: 4em;
          }
          40% {
            box-shadow: 0 -2em;
            height: 5em;
          }
        }
        @keyframes load1 {
          0%,
          80%,
          100% {
            box-shadow: 0 0;
            height: 4em;
          }
          40% {
            box-shadow: 0 -2em;
            height: 5em;
          }
        }
      `}</style>
    </div>
  );
};

export default FullPageSpinner;
