import React from "react";
import CardCollection from "../state/CardCollection";

/**
 * A single card for Elder Scrolls Legends
 */
const Card: React.FC<CardCollection.Card> = props => {
  return (
    <>
      <article className="card" id={props.id}>
        <h2 className="name">{props.name}</h2>
        <div className="type">
          <span className="label">type</span>
          {props.type}
        </div>
        <div className="setName">
          <span className="label">set</span>
          {props.set.name}
        </div>
        <img src={props.imageUrl} alt={`${props.name} card image`}></img>
        <div className="text">
          {props.text && <span className="label">text</span>}
          {props.text}
        </div>
      </article>
      <style jsx>{`
          .card {
            border: 1px solid var(--font-color)
            border-radius: 5px;
            box-shadow: 2px 1px 5px #333;
            background: #222;
            padding: 1rem;
            color: var(--font-color);
            display: grid;
            align-items: start;
            grid-template-columns: repeat(2, 1fr);
            grid-auto-rows: min-content;
            text-align: center;
            grid-template-areas: 
              "image image"
              "name name"
              "type setName"
              "text text"
          }

          .card > * {
            width: 100%;
            padding: .5rem 0;
            display: flex;
            flex-flow: column;
            align-items: center;
            justify-content: center;
          }
          
          .card > *:not(:last-child) {
            border-bottom: 1px solid var(--font-color);
          }

          .name {
            grid-area: name;
            font-size: 1.5rem;
            font-weight: 700;
            margin: 0;
            text-align: center;
          }
          
          .type {
            grid-area: type;
            border-right: 1px solid var(--font-color);
          }
          
          .setName, .type, .text {
            min-height: 3.5rem;
          }

          .setName {
            grid-area: setName;
            border-left: 1px solid var(--font-color);
          }

          .label {
            font-size: 0.7rem;
            filter: brightness(.8);
            text-transform: uppercase;
            font-family: sans-serif;
          }

          img {
            height: auto;
            grid-area: image;
            min-height: 400px;
          }

          .text {
            grid-area: text;
            padding: .5rem 0;
          }

        `}</style>
    </>
  );
};

export default Card;
