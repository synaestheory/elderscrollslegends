import React from "react";
import axios from "axios";

/**
 * This namespace represents the application state for an Elder Scrolls Legends
 * card collection.
 */
namespace CardCollection {
  enum ActionType {
    CARDS_PAGE_RECEIVED = "CARDS_PAGE_RECEIVED",
    CARDS_PAGE_REQUESTED = "CARDS_PAGE_REQUESTED",
    CARDS_PAGE_REQUEST_FAILED = "CARDS_PAGE_REQUEST_FAILED"
  }

  /**
   * API successfully returns a card page
   */
  interface CardsPageReceived {
    type: ActionType.CARDS_PAGE_RECEIVED;
    payload: ApiResponse;
  }
  /**
   * A cards page is being requested from the API
   */
  interface CardsPageRequested {
    type: ActionType.CARDS_PAGE_REQUESTED;
  }

  /**
   * A cards page request has failed
   */
  interface CardsPageRequestFailed {
    type: ActionType.CARDS_PAGE_REQUEST_FAILED;
  }

  type Action = CardsPageReceived | CardsPageRequested | CardsPageRequestFailed;

  /**
   * This is a single card
   */
  export interface Card {
    /**
     * The card name.
     */
    name: string;
    /**
     * The rarity of the card (ex. Common, Epic, Rare, Legendary)
     */
    rarity: string;
    /**
     * The type of the card (ex. Action, Creature, Item, Support)
     */
    type: string;
    /**
     * The subtypes of the card (ex. Argonian, Dragon, Imperial)
     */
    subtypes: string[];
    /**
     * How much it costs to cast the card
     */
    cost: number;
    /**
     * How much damage the card can do
     */
    power: number;
    /**
     * The total health of the card
     */
    health: number;
    set: {
      /**
       * The id of the set the card belongs to
       */
      id: string;
      /**
       * The name of the set the card belongs to
       */
      name: string;
      /**
       * API Endpoint to the set the card belongs to
       */
      _self: string;
    };
    /**
     * The number of soul gems it costs to soul summon
     */
    soulSummon: number;
    /**
     * The number of soul gems it costs to soul trap
     */
    soulTrap: number;
    /**
     * Rules of the card
     */
    text: string;
    /**
     * The attributes of the card (ex. Agility, Endurance, Intelligence)
     */
    attributes: string[];
    /**
     * The keywords of the card (ex. Assemble, Breakthrough, Charge)
     */
    keywords: string[];
    /**
     * Is the card unique? Unqiue cards can only be included once per deck.
     */
    unique: true;
    /**
     * The image url for the card.
     */
    imageUrl: string;
    /**
     * A unique id for this card. It is made up via a SHA1 of the {card name}-{set name}
     */
    id: string;
  }

  /**
   * This interface represents the response body of the Elder Scrolls Legends API
   */
  interface ApiResponse {
    cards: Card[];
    /**
     * The page size of the response
     */
    _pageSize: number;
    /**
     * The total number of pages
     */
    _totalCount: number;
    _links: {
      prev?: string;
      next?: string;
    };
  }

  /**
   * This is a boilerplate interface. It represents the values handed from the reducer to the context.
   */
  interface ContextState {
    state: State;
    dispatch?: React.Dispatch<Action>;
  }

  /**
   * This is the state of the card collection
   */
  interface State {
    /**
     * All of the cards currently in the collection
     */
    cards: Card[];
    /**
     * This indicates whether or not there is a request currently pending to the API
     */
    requestPending: boolean;
    /**
     * The url of the next page to fetch
     */
    _nextPage: string;
    /**
     * The number of cards per page to fetch
     */
    _pageSize: number;
  }

  export const INITIAL_STATE: State = {
    cards: [],
    requestPending: false,
    _nextPage: "https://api.elderscrollslegends.io/v1/cards",
    _pageSize: 20
  };

  export const Context: React.Context<ContextState> = React.createContext({
    state: INITIAL_STATE
  });

  export const Reducer: React.Reducer<State, Action> = (state, action) => {
    switch (action.type) {
      case ActionType.CARDS_PAGE_REQUESTED:
        return {
          ...state,
          requestPending: true
        };
      case ActionType.CARDS_PAGE_RECEIVED:
        return {
          cards: Array.from(new Set(state.cards.concat(action.payload.cards))),
          _nextPage: action.payload._links.next,
          _pageSize: action.payload._pageSize,
          requestPending: false
        };
      case ActionType.CARDS_PAGE_REQUEST_FAILED:
        return {
          ...state,
          requestPending: false,
          _lastPageRequested: undefined
        };
      default:
        return state;
    }
  };
  /**
   * CardCollectionApi contains the card collection, the ability to make requests to add more cards to the collection
   * and infromation on whether or not there is currently a request for more cards.
   */
  export interface CardCollectionApi {
    /**
     * All cards currently in the collection
     */
    cards: Card[];
    /**
     * True if there is a request pending with the Elder Scrolls Legends Api
     */
    requestPending: boolean;
    /**
     * Fetches the next page of cards and adds them to the list
     */
    fetchNextPage: () => void;
  }

  /**
   * This is a custom hook that provides access to the card collection
   * and APIs to interact with it.
   */
  export const useApi = (): CardCollectionApi => {
    const { state, dispatch } = React.useContext(Context);
    const requestRef = React.useRef<string>();
    const fetchNextPage = React.useCallback(() => {
      /**
       * If there is no _nextPage or there is currently a
       * request pending, do nothing.
       */
      if (
        state._nextPage === undefined ||
        requestRef.current === state._nextPage
      ) {
        return;
      }
      requestRef.current = state._nextPage;
      dispatch({ type: ActionType.CARDS_PAGE_REQUESTED });
      axios
        .get(state._nextPage, {
          params: { pageSize: state._pageSize }
        })
        .then(response =>
          dispatch({
            type: ActionType.CARDS_PAGE_RECEIVED,
            payload: response.data
          })
        )
        .catch(error => {
          console.error("Error requesting cards page", { error });
          dispatch({ type: ActionType.CARDS_PAGE_REQUEST_FAILED });
        })
        .finally(() => (requestRef.current = undefined));
    }, [state._nextPage, state._pageSize]);
    return {
      cards: state.cards,
      requestPending: state.requestPending,
      fetchNextPage
    };
  };
}

export default CardCollection;
